---
  title: Govern
  description: Manage security vulnerabilities, policies, and compliance across your organization.
  components:
    - name: sdl-cta
      data:
        title: Govern
        aos_animation: fade-down
        aos_duration: 500
        subtitle: Manage security vulnerabilities, policies, and compliance across your organization.
        text: |
          GitLab helps users manage security vulnerabilities, policies, and compliance across their organization.
        icon:
          name: protect-alt-2
          alt: Govern Spotlight Icon
          variant: marketing
    - name: featured-media
      data:
        column_size: 6
        header: Product categories
        header_animation: fade-up
        header_animation_duration: 500
        media:
          - title: Security Policies
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Unified security policy management capabilities across all of GitLab's scanners and security technologies. Apply policies to enforce scans and to require security approvals when vulnerabilities are found.
            link:
              href: https://docs.gitlab.com/ee/user/application_security/threat_monitoring/#configuring-network-policy-alerts
              text: Learn More
              data_ga_name: security orchestation learn more
              data_ga_location: body
          - title: Vulnerability Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              View, triage, trend, track, and resolve vulnerabilities detected in your applications.
            link:
              href: https://docs.gitlab.com/ee/user/application_security/security_dashboard/
              text: Learn More
              data_ga_name: vulnerability management learn more
              data_ga_location: body
          - title: Audit Events
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Track important events for review and compliance such as who performed certain actions and the time they happened.
            link:
              href: https://docs.gitlab.com/ee/administration/audit_events.html
              text: Learn More
              data_ga_name: audit events learn more
              data_ga_location: body
          - title: Compliance Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Provide customers with the tools and features necessary to manage their compliance programs.
            link:
              href: https://docs.gitlab.com/ee/administration/compliance.html
              text: Learn More
              data_ga_name: compliance management learn more
              data_ga_location: body
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - align_center: true
            text: |
              Learn more about our roadmap for upcoming features on our [Direction page](/direction/govern/){data-ga-name="govern direction" data-ga-location="body"}.
    - name: sdl-related-card
      data:
        column_size: 4
        header: Related
        cards:
          - title: Release
            icon:
              name: release-alt-2
              alt: Release Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 500
            text: |
              GitLab's integrated CD solution allows you to ship code with zero-touch, be it on one or one thousand servers.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/release/
              data_ga_name: release
              data_ga_location: body
          - title: Secure
            icon:
              name: secure-alt-2
              alt: Secure Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1000
            text: |
              Security capabilities, integrated into your development lifecycle.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/secure/
              data_ga_name: secure
              data_ga_location: body
          - title: Manage
            icon:
              name: manage-alt-2
              alt: Manage Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1500
            text: Gain visibility and insight into how your business is performing.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/manage/
              data_ga_name: manage
              data_ga_location: body
